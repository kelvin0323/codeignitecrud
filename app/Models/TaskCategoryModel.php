<?php

namespace App\Models;

use CodeIgniter\Model;

class TaskCategoryModel extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'task_category';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'array';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = [
		"task_id",
		"category_id",
	];
}
