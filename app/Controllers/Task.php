<?php 
namespace App\Controllers;
use App\Models\UserModel;
use App\Models\TaskModel;
use App\Models\CategoryModel;
use App\Models\TaskCategoryModel;

class Task extends BaseController
{
	public function index()
	{
		$data = [];
		$taskModel = new TaskModel();

		$tasks = $taskModel->where('user_id',session()->get('id'))->orderBy('id','desc')->findAll();

		foreach($tasks as $key => $task){
			$categories = $taskModel->where('id',$task['id'])->getCategories($task['id']);
			foreach($categories as $c){
				$tasks[$key]['categories'][] = $c->name;
			}
		}
		// var_dump($tasks[0]);
		// exit();
		$data['tasks'] = $tasks;
		return view('tasks/index',$data);
	}

	public function create()
	{
		$data = [];
		$catModel = new CategoryModel();

		if ($this->request->getMethod() == 'post') {
			$rules = [
				'title' => 'required',
				'description' => 'required',
				'categories' => 'required|is_array',
			];

			$messages = [
				'categories' => [
					'is_array' => 'Categories must be an array format'
				]
			];

			if (! $this->validate($rules,$messages)) {
				$data['validation'] = $this->validator;
			}else{

				$taskModel = new TaskModel();

				$newData = [
					'user_id' => session()->get('id'),
					'title' => $this->request->getPost('title'),
					'description' => $this->request->getPost('description')
				];

				$taskModel->save($newData);

				$taskCatModel = new TaskCategoryModel();
				foreach($this->request->getPost('categories') as $cid){
					$newTaskCatData = [
						'task_id' => $taskModel->orderBy('id','desc')->first()['id'],
						'category_id' => $cid
					];
					$taskCatModel->save($newTaskCatData);
				}

				session()->setFlashdata('success', 'Successfuly Added');
				return redirect()->to('/task');			
			}
		}

		$data['categories'] = $catModel->where('user_id',$this->user['id'])->findAll();
		return view('tasks/add',$data);
	}

	public function edit($id)
	{
		$data = [];
		$taskModel = new TaskModel();
		$catModel = new CategoryModel();

		$task = $taskModel->where('id',$id)->first();
		if ($this->request->getMethod() == 'post') {

			$rules = [
				'title' => 'required',
				'description' => 'required',
				'categories' => 'required|is_array',
			];

			$messages = [
				'categories' => [
					'is_array' => 'Categories must be an array format'
				]
			];

			if (! $this->validate($rules,$messages)) {
				$data['validation'] = $this->validator;
			}else{
				$newData = [
					'id' => $task['id'],
					'title' => $this->request->getPost('title'),
					'description' => $this->request->getPost('description')
				];

				$taskModel->save($newData);

				$taskCatModel = new TaskCategoryModel();
				$newTaskCatData = [];
				$taskCatModel->where('task_id',$id)->delete();
				foreach($this->request->getPost('categories') as $cid){
					$newTaskCatData = [
						'task_id' => $id,
						'category_id' => $cid
					];
					$taskCatModel->save($newTaskCatData);
				}


				session()->setFlashdata('success', 'Successfuly Updated');
				return redirect()->to('/task');			
			}
		}

		$categories = $taskModel->where('id',$id)->getCategories($id);
		foreach($categories as $c){
			$task['categories']['id'][] = $c->id;
		}


		$data['task'] = $task;
		$data['categories'] = $catModel->where('user_id',$this->user['id'])->findAll();
		return view('tasks/edit',$data);
	}

	public function delete($id)
	{
        $taskModel = new TaskModel();
        $taskCatModel = new TaskCategoryModel();

        $taskModel->delete($id);
        $taskCatModel->where('task_id',$id)->delete();

        $session = session();
        $session->setFlashdata("success", "Task deleted successfully");

        return redirect()->to('/task');
	}
}
