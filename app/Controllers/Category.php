<?php 
namespace App\Controllers;
use App\Models\UserModel;
use App\Models\CategoryModel;
use App\Models\TaskModel;
use App\Models\TaskCategoryModel;

class Category extends BaseController
{
	public function index()
	{
		$data = [];
		$catModel = new CategoryModel();

		$data['categories'] = $catModel->where('user_id',$this->user['id'])->orderBy('id','desc')->findAll();
		return view('categories/index',$data);
	}

	public function create()
	{
		$data = [];

		if ($this->request->getMethod() == 'post') {

			$rules = [
				'name' => 'required',
			];

			if (! $this->validate($rules)) {
				$data['validation'] = $this->validator;
			}else{

				$catModel = new CategoryModel();

	            $newData = [
	            	'user_id' => $this->user['id'],
	                'name' => $this->request->getPost('name'),
	            ];

	            $catModel->save($newData);

	            session()->setFlashdata('success', 'Successfuly Added');
	            return redirect()->to('/category');			
			}
		}

		return view('categories/add',$data);
	}

	public function edit($id)
	{
		$data = [];
		$catModel = new CategoryModel();
		$category = $catModel->where('id',$id)->first();
		if ($this->request->getMethod() == 'post') {
			$rules = [
				'name' => 'required',
			];

			if (! $this->validate($rules)) {
				$data['validation'] = $this->validator;
			}else{
				
				$newData = [
					'id' => $category['id'],
					// 'user_id' => $this->user['id'],
					'name' => $this->request->getPost('name'),
				];

				$catModel->save($newData);

				session()->setFlashdata('success', 'Successfuly Updated');
				return redirect()->to('/category');			
			}
		}

		$data['category'] = $category;
		return view('categories/edit',$data);
	}

	public function delete($id)
	{
        $catModel = new CategoryModel();
        $taskCatModel = new TaskCategoryModel();
        
        $catModel->delete($id);
        $taskCatModel->where('category_id',$id)->delete();

        $session = session();
        $session->setFlashdata("success", "Task deleted successfully");

        return redirect()->to('/category');
	}
}
