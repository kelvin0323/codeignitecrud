<?= $this->extend("templates/header") ?>

<?= $this->section("body") ?>
<style>
	.create-btn{
		float: right!important;
		margin-top: 45px;
		/*display: inline-block;*/
	}

	.d-i-b{
		display: inline-block!important;
	}
</style>
<div class="container">
	<div class="row">
		<div class="col-12">
			<h4 class="mt-5 d-i-b">Categories List</h4>
			<div class="create-btn">
				<a class="btn btn-primary" href="/category/create">Create</a>
			</div>
			<div class="card card-default mt-2">
				<div class="body">
					<table class="table">
						<thead>
							<tr>
								<th scope="col">ID</th>
								<th scope="col">Name</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach ($categories as $category) {
								?>
							<tr>
								<th scope="row"><?php echo $category['id']; ?></th>
								<td><?php echo $category['name']; ?></td>
								<td>
                                    <a href="<?= base_url('category/edit/' . $category['id']); ?>" class="btn btn-info">Edit</a>
                                    <a href="<?= base_url('category/delete/' . $category['id']); ?>" class="btn btn-danger" onclick="return confirm('Are you sure want to delete?')">Delete</a>
								</td>
							</tr>
							<?php
							}
							?>
						</tbody>
					</table>	
				</div>
			</div>
		</div>
	</div>
</div>

<?= $this->endSection() ?>