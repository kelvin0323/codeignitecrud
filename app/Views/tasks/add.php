<?= $this->extend("templates/header") ?>

<?= $this->section("body") ?>

<style>

</style>
<div class="container">
    <div class="row">
        <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 mt-5 pt-3 pb-3 bg-white from-wrapper">
            <?php if (isset($validation)): ?>
                <div class="col-12 text-left">
                    <div class="alert alert-danger" role="alert">
                        <?= $validation->listErrors() ?>
                    </div>
                </div>
            <?php endif; ?>
            <div class="container">

                <form action="/task/create" method="post">
                    <div class="row">
                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" class="form-control" name="title" id="title">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <label for="title">Description</label>
                                <textarea type="text" class="form-control" name="description"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-sm-12">
                            <label for="categories">Categories</label>
                            <div class="form-group">
                                <?php foreach($categories as $c) { ?>
                                    <div class="form-check form-check-inline">

                                        <input class="form-check-input"
                                            type="checkbox" name="categories[]" value="<?= $c['id'] ?>">
                                        <label class="form-check-label" for="inlineCheckbox1"><?= $c['name'] ?></label>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-sm-4">
                            <button type="submit" class="btn btn-primary">Add</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>