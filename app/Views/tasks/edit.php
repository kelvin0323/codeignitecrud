<?= $this->extend("templates/header") ?>

<?= $this->section("body") ?>
<div class="container">
    <div class="row">
        <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 mt-5 pt-3 pb-3 bg-white from-wrapper">
            <div class="container">
                <?php if (isset($validation)): ?>
                    <div class="col-12 text-left">
                        <div class="alert alert-danger" role="alert">
                            <?= $validation->listErrors() ?>
                        </div>
                    </div>
                <?php endif; ?>
                <form class="" action="/task/edit/<?= $task['id'] ?>" method="post">
                    <div class="row">
                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" class="form-control" name="title" id="firstname" value="<?= $task['title'] ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <label for="title">Description</label>
                                <textarea type="text" class="form-control" name="description"><?= $task['description'] ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12">
                            <label for="categories">Categories</label>
                            <div class="form-group">
                                <?php foreach($categories as $c) { ?>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input"
                                            <?php if(isset($task['categories']['id']) && in_array($c['id'], $task['categories']['id'])) { ?>
                                                checked
                                            <?php } ?>
                                            type="checkbox" name="categories[]" value="<?= $c['id'] ?>">
                                        <label class="form-check-label" for="inlineCheckbox1"><?= $c['name'] ?></label>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-sm-4">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>