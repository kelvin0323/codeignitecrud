<?= $this->extend("templates/header") ?>

<?= $this->section("body") ?>

<style>
	.create-btn{
		float: right!important;
		margin-top: 45px;
		/*display: inline-block;*/
	}

	.d-i-b{
		display: inline-block!important;
	}
</style>
<div class="container">
	<div class="row">
		<div class="col-12">
			<h4 class="mt-5 d-i-b">Tasks List</h4>
			<div class="create-btn">
				<a class="btn btn-primary" href="/task/create">Create</a>
			</div>
			<div class="card card-default mt-2">
				<div class="body">
					<table class="table">
						<thead>
							<tr>
								<th scope="col">ID</th>
								<th scope="col">Title</th>
								<th scope="col">Description</th>
								<th scope="col">Categories</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach ($tasks as $task) {
								?>
							<tr>
								<th scope="row"><?php echo $task['id']; ?></th>
								<td><?php echo $task['title']; ?></td>
								<td><?php echo $task['description']; ?></td>
								<td>
									<?php if(isset($task['categories'])) foreach ($task['categories'] as $c) { ?>
										<button class="btn btn-primary btn-sm"><?php echo $c; ?></button>
									<?php  } ?>		
								</td>
								<td>
                                    <a href="<?= base_url('task/edit/' . $task['id']); ?>" class="btn btn-info">Edit</a>
                                    <a href="<?= base_url('task/delete/' . $task['id']); ?>" class="btn btn-danger" onclick="return confirm('Are you sure want to delete?')">Delete</a>
								</td>
							</tr>
							<?php
							}
							?>
						</tbody>
					</table>	
				</div>
			</div>
		</div>
	</div>
</div>

<?= $this->endSection() ?>