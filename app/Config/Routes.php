<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('User');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/',function(){
	return redirect()->to(base_url('dashboard'));
});

$routes->get('logout', 'User::logout');
$routes->get('login', 'User::index', ['filter' => 'noauth']);
$routes->match(['get','post'],'register', 'User::register', ['filter' => 'noauth']);
$routes->match(['get','post'],'profile', 'User::profile',['filter' => 'auth']);
$routes->match(['get','post'],'task', 'Task::index',['filter' => 'auth']);
$routes->match(['get','post'],'task/create', 'Task::create',['filter' => 'auth']);
$routes->match(['get','post'],'task/edit/{id}', 'Task::edit',['filter' => 'auth']);
$routes->post('task/delete/{id}', 'Task::delete',['filter' => 'auth']);

$routes->match(['get','post'],'category', 'Category::index',['filter' => 'auth']);
$routes->match(['get','post'],'category/create', 'Category::create',['filter' => 'auth']);
$routes->match(['get','post'],'category/edit/{id}', 'Category::edit',['filter' => 'auth']);
$routes->post('category/delete/{id}', 'Category::delete',['filter' => 'auth']);

$routes->match(['get','post'],'category', 'Category::index',['filter' => 'auth']);
$routes->get('dashboard', 'Dashboard::index',['filter' => 'auth']);

if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
