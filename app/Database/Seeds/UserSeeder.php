<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class UserSeeder extends Seeder
{
	public function run()
	{
        $model = model('UserModel');

        $model->insert([
            'firstname' => 'Kelvin',
            'lastname' => 'Tan',
            'email' => 'example@gmail.com',
            'password' => 'password',
        ]);
	}
}
