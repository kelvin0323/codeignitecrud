<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class TaskSeeder extends Seeder
{
	public function run()
	{
        $model = model('TaskModel');

        $model->insert([
            'user_id' => '1',
            'title' => 'DLIdeas',
            'description' => 'Nice to meet you!',
        ]);

        $model->insert([
            'user_id' => '1',
            'title' => 'This is a title',
            'description' => 'This is a description',
        ]);
	}
}
