<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class CategorySeeder extends Seeder
{
	public function run()
	{
        $model = model('CategoryModel');

        $model->insert([
            'user_id' => '1',
            'name' => 'Remind Me Later',
        ]);

        $model->insert([
            'user_id' => '1',
            'name' => 'Hobby',
        ]);

        $model->insert([
            'user_id' => '1',
            'name' => 'Important',
        ]);

        $model->insert([
            'user_id' => '1',
            'name' => 'Music',
        ]);

        $model->insert([
            'user_id' => '1',
            'name' => 'Mood',
        ]);
	}
}
