<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class TaskCategorySeeder extends Seeder
{
	public function run()
	{
        $model = model('TaskCategoryModel');

        $model->insert([
            'task_id' => '1',
            'category_id' => '1',
        ]);

        $model->insert([
            'task_id' => '1',
            'category_id' => '2',
        ]);

        $model->insert([
            'task_id' => '1',
            'category_id' => '3',
        ]);

        $model->insert([
            'task_id' => '2',
            'category_id' => '3',
        ]);

        $model->insert([
            'task_id' => '2',
            'category_id' => '4',
        ]);
	}
}
