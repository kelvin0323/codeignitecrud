<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class TaskCategory extends Migration
{
	public function up()
	{
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'task_id' => [
                'type' => 'INT',
                'constraint' => '100',
                'null' => false,
            ],
            'category_id' => [
                'type' => 'INT',
                'constraint' => '100',
                'null' => false,
            ],
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('task_category');
	}

	public function down()
	{
		$this->forge->dropTable('task_category');
	}
}
